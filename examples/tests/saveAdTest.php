<?php
require_once('testBootstrap.php');


$post = array(
	'inzerazId' => 24,
	'title' => 'Predam iPhone 5',
	'description' => "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.",
	'category' => 3,
	'type' => 1,
	'condition' => 2,
	'priceType' => 1,
	'price' => 599,
	'locality' => 24,
	'images' => array(
		'http://placehold.it/350x150'
	),
	'inzerazUserId' => 2,
	'userName' => 'Peter',
	'userEmail' => 'email@email.sk',
	'userPhone' => '0908 123 456',
	'inzerazData' => array(
		'category' => 21,
		'type' => 1,
		'condition' => 3,
		'priceType' => 2,
	)
);

$post = json_encode($post);
$url = $baseUrl . '?' . http_build_query(array('action' => 'save-ad', 'token' => Inzeraz::getToken($post)));
$response = post($url, array('data' => $post));

printResponse($response);

