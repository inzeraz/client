<?php
require_once('../../src/Inzeraz.php');


Inzeraz::init('12345', 'toto-je-super-tajny-kluc');

$baseUrl = 'http://127.0.0.1/inzeraz-client/examples/index.php';




function post($url, array $post)
{
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$server_output = curl_exec ($ch);
	curl_close ($ch);

	return $server_output;
}


function printResponse($response)
{
	$responseArray = json_decode($response, TRUE);

	if($responseArray) {
		pre($responseArray);
	} else {
		echo '<h1>Invalid JSON:</h1>';
		var_dump($response);
		echo '<h1>Response:</h1><hr>';
		pre($response);
	}
}

function pre($string)
{
	if(is_array($string)) $string = print_r($string, TRUE);
	echo "<pre>{$string}</pre>";
}
