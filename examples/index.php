<?php

require_once __DIR__ . '/../vendor/autoload.php';

date_default_timezone_set('Europe/Prague');


$app = new Slim(); // Slim kniznicu stiahnete na https://github.com/codeguy/Slim

Inzeraz::init('12345', 'toto-je-super-tajny-kluc'); // nastavime appId a appSecret

$app->post('/', function () use ($app) {

	$parameters = $app->request()->get(); // ziskame vsetky $_GET parametre
	$parameters['data'] = $app->request()->post('data'); // ziskame vsetky $_POST parametre

	// overime spravnost requestu
	$data = Inzeraz::verifyRequest($parameters);
	if(is_array($data)) {
		// pokracujeme len ak je premenna $data pole !!!


		$error = Inzeraz::ERROR_NONE;

		if($parameters['action'] == Inzeraz::ACTION_SAVE_AD) {

			//
			// @TODO: sprasujeme $data o inzerate, ulozime do DB
			//

			// pripravime data o novospracovanom inzerate
			$responseData = Inzeraz::adInfo($data['inzerazId'], 43, 'http://napredaj.local/ad/43', Inzeraz::AD_STATUS_LIVE, strtotime('+6 week'));

		} else if ($parameters['action'] == Inzeraz::ACTION_DEACTIVATE_AD) {
			$adIds = $data; // pole s ID-ckami inzeratov kt. treba zmazat / deaktivovat

			$responseData = array();
			foreach($adIds as $inzerazId) {
				//
				// @TODO: zmazat / deaktivovat inzeraty v DB
				//
				$responseData[] = Inzeraz::adNotExistsInfo($inzerazId);
			}

		} else if ($parameters['action'] == Inzeraz::ACTION_AD_INFO) {
			$adIds = $data; // pole s ID-ckami inzeratov

			//
			// @TODO: vybrat inzeraty z DB
			//

			$database = array(
				1 => array(42, 'http://napredaj.local/ad/42', Inzeraz::AD_STATUS_DRAFT, strtotime('+6 week'), FALSE, 13),
				4 => array(43, 'http://napredaj.local/ad/43', Inzeraz::AD_STATUS_LIVE, strtotime('+6 week'), FALSE, 302),
				5 => array(44, 'http://napredaj.local/ad/44', Inzeraz::AD_STATUS_LIVE, strtotime('+6 week'), TRUE, 503),
			);

			// potom zostavime priblizne taketo pole
			$responseData = array();
			foreach($adIds as $inzerazId) {
				if(isset($database[$inzerazId])) { // ak je inzerat v DB tak posleme adInfo() ak nieje tak adNotExistsInfo()
					$adInfo = $database[$inzerazId];
					$responseData[] = Inzeraz::adInfo($inzerazId, $adInfo[0], $adInfo[1], $adInfo[2], $adInfo[3], $adInfo[4], $adInfo[5]);
				} else {
					$responseData[] = Inzeraz::adNotExistsInfo($inzerazId);
				}
			}

		} else {
			$responseData = array();
			$error = Inzeraz::ERROR_INVALID_ACTION; // nastavime error kod
		}

		$token = Inzeraz::getToken($responseData);

		$response = array(
			'error' => $error,
			'data' => $responseData,
			'token' => $token,
		);

		$app->contentType('application/json'); // nastavime Content-Type: application/json
		$app->response()->body(json_encode($response)); // vypiseme json
	} else {
		exit('exit'); // ak je token napravny ignorujeme request
	}


});


$app->run();
