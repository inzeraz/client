<?php


class Inzeraz
{

	const ACTION_SAVE_AD = 'save-ad'; // pridanie / aktualizovanie inzeratu
	const ACTION_DEACTIVATE_AD = 'deactivate-ad'; // zmazanie inzeratu
	const ACTION_AD_INFO = 'ad-info'; // overovanie existujucich inzeratov

	const ERROR_NONE = 0;
	const ERROR_INVALID_ACTION = 101;

	const AD_STATUS_LIVE = 'live'; // ak sa inzerat zobrazuje na stranke
	const AD_STATUS_DRAFT = 'draft'; // ak sa inzerat NEzobrazuje na stranke
	const AD_STATUS_NOT_EXISTS = 'not-exists'; // ak sa inzerat neexistule na vasej stranke


	private static $appId;
	private static $appSecret;


	/**
	 * Static class - cannot be instantiated.
	 */
	final public function __construct()
	{
		throw new LogicException("Cannot instantiate static class " . get_class($this));
	}


	/**
	 * @param $appId
	 * @param $appSecret
	 */
	public static function init($appId, $appSecret)
	{
		self::$appId = $appId;
		self::$appSecret = $appSecret;
	}


	public static function mustBeInitialized()
	{
		if(!self::$appId or !self::$appSecret) {
			throw new RuntimeException('Inzeraz is not initialized, call Inzeraz::init() first');
		}
	}


	/**
	 * @param $inzerazId
	 * @param int $id
	 * @param string $url Url na ktorej je detaul inzeratu
	 * @param string $status Status ci sa inzerat zobrazuje na stranke
	 * @param int $expire Timestamp do kedy je inzerat zverejneny
	 * @param bool $featured Je inzerat topovany ?
	 * @param int|null $viewsCount Kolko ludi si zobrazilo inzerat ?
	 *
	 * @return array
	 */
	public static function adInfo($inzerazId, $id, $url, $status, $expire = NULL, $featured = FALSE, $viewsCount = NULL)
	{
		self::mustBeInitialized();
		return array(
			'type' => 'ad-info',
			'inzerazId' => $inzerazId,
			'id' => $id,
			'url' => $url,
			'status' => $status,
			'expire' => $expire,
			'featured' => $featured,
			'viewsCount' => $viewsCount,
		);
	}


	/**
	 * @param $inzerazId
	 *
	 * @return array
	 */
	public static function adNotExistsInfo($inzerazId)
	{
		self::mustBeInitialized();
		return array(
			'type' => 'ad-info',
			'inzerazId' => $inzerazId,
			'status' => self::AD_STATUS_NOT_EXISTS,
		);
	}


	/**
	 * @param array $parameters
	 *
	 * @return bool|array
	 */
	public static function verifyRequest(array $parameters)
	{
		self::mustBeInitialized();
		if(isset($parameters['token']) and isset($parameters['data']) and isset($parameters['action'])) {
			$action = $parameters['action'];
			$token = $parameters['token'];
			$data = $parameters['data'];

			if(self::isTokenValid($token, $data)) {
				return json_decode($data, TRUE);
			}
		}

		return FALSE;
	}


	/**
	 * @param string $token
	 * @param string $data
	 *
	 * @return bool
	 */
	public static function isTokenValid($token, $data)
	{
		self::mustBeInitialized();
		return $token == self::getToken($data);
	}


	/**
	 * @param $data
	 *
	 * @return bool|string
	 */
	public static function getToken($data)
	{
		self::mustBeInitialized();
		$token = self::sha1(array($data, self::$appId, self::$appSecret));
		return substr($token, 0, 48);
	}


	/**
	 * @param array|string $data
	 *
	 * @return string
	 */
	public static function sha1($data)
	{
		self::mustBeInitialized();
		if(is_array($data)) {
			$data = json_encode($data);
		}
		return sha1($data);
	}

}

