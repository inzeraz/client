# Inzeraz.sk API

## Inštalácia


1. Stiahnite si aktuálnu [verziu](https://bitbucket.org/inzeraz/client/get/master.zip).
2. vo vašej aplikácii načítajte súbor `/src/Inzeraz.php` pomocou funkcie `require_once`
3. inicializujte triedu Inzeraz pomocou metody `Inzeraz::init($appId, $appSecret)`



## Pouzitie

Aktuálne sa posielajú 3 druhy requestov z Inzeraz.sk na vašu stránku, a to:
- Pridanie inzeratu `\Inzeraz::ACTION_SAVE_AD`
- Zmazanie inzeratu `\Inzeraz::ACTION_DEACTIVATE_AD`
- Ziskanie info o inzerate `\Inzeraz::ACTION_AD_INFO`


Na pozadovanu URL posleme GET request s parametrami `action`, `token` a `data`
- **action** : typ requestu
- **token** : token na zaklade kt. sa verifukjú dáta
- **data** : cryptovane dáta


Každý request je **potrebné overiť** , na to nam služi metóda `\Inzeraz::verifyRequest()` do ktorej ako prvý parameter vložte všetky $_GET premenné, metoda vracia **pole ak je request OK** ak nieje vrati `false`.

Na kazdy request poslite response ako pole v JSON formate. Pole nusi obsahovat nasledovne kluce:
- **error** ake je vsetko OK tak nastavte na `\Inzeraz::ERROR_NONE` v opacnom pripade pouzite `\Inzeraz::ERROR_INVALID_ACTION`
- **data** obsahuje samotne data
- **token** ziskate pomocou metody `\Inzeraz::getToken()` kde ako prvy parameter vlozite **data**


### Pridanie inzeratu
```
#!php
$requestData = \Inzeraz::verifyRequest($data);
```
Ak je request OK tak premenna `$requestData` bude obsahovat pole s informaciami o novopridanom inzerate.

 nazov klucu | typ hodnoty | popis
 ----------- | ----------- | ----
 inzerazId | int | ID inzeratu v inzeraz.sk **toto id si ulozte k inzeratu**
 title | string | nazov
 description | string | popis
 category | int | kategoria
 type | int | typ inzeratu (kupa / predaj)
 condition | int | stav produktu (novy / pouziti / ...)
 priceType | int | typ ceny (suma / dohodou / ...)
 price | float | konkretna suma
 locality | int | obec
 images | array | pole s cestami ku obrazkom
 inzerazUserId | int | ID uzivatela v inzeraz.sk
 userName | string | mano uzivatela
 userEmail | string | email uzivatela
 userPhone | string | tel. cislo uzivatela
 inzerazData | array | pole

Po tom ako si overite spravnost requestu mozete ulozit novy inzerat do vasej databazi.
Resnponse poslite vystup s metody `\Inzeraz::adInfo()`

### Zmazanie inzeratu
```
#!php
$requestData = \Inzeraz::verifyRequest($data);
```
Ak je request OK tak premenna `$requestData` bude obsahovat pole s inzeraz.sk ID-ckami.
Po usposnom zazani poslite responze ako pole s potvrdenim o zmazani:
```
#!php
$responseData = array(
	\Inzeraz::adNotExistsInfo($inzerazId),
	\Inzeraz::adNotExistsInfo($inzerazId),
	// ...
);
```

### Ziskanie info o inzerate
```
#!php
$requestData = \Inzeraz::verifyRequest($_GET);
```
Ak je request OK tak premenna `$requestData` bude obsahovat pole s inzeraz.sk ID-ckami.
Response poslite ako pole s informaciami o inzerate:
```
#!php
$responseData = array(
	\Inzeraz::adInfo(...),
	\Inzeraz::adInfo(...),
	\Inzeraz::adNotExistsInfo($inzerazId),
	// ...
);
```
Ako je vidiet na priklade pole moze obsahovat samotne info o inzerate alebo info o tom ze inzerat neexistuje


## Priklad
Kompletny priklad najdete v subore [/examples](https://bitbucket.org/inzeraz/client/src/master/examples/index.php)

za predpokladu ze inicializuje triedu Inzeraz ako je to uvedene v priklade: `\Inzeraz::init('12345', 'toto-je-super-tajny-kluc');`
mozete dostavat dole uvedene Requesty a vasa aplikacia by mala posielat podobny Response ako je v priklade

### Pridanie inzeratu
Request:
```
http://napredaj.local/api/save-ad?action=save-ad&token=VlJMTER2RHQxaFJhSUlGR045aVFSSmp0ZUNwT1VNQ2JuVkM3&data=R0JZaFI1VGFPOGhrckZkODlieGdZWXFwV1h5eExHaWM5dzBnS2tTYU85cnk1Und3eldNZmQ4ZHBOTUJQOCtFaWJLV0dBeUhabEQvUUNIc3M4UTNZakhXeWpQdkdLVkdxb2hjMktBRUcvNC9Hd2o0QnRwcEs2Y2E2QlRjL2JzOElQb1Y2NzdjNnhVbGU0QWJtSExYeEl1TmY0ZklXZjhEK3kxY3VGMjNNK0d2bktEdVRrMVBXVjdxVkNhVVBuZmVtQ1V5bmJjY1I1aXppb0tSODUvZnhTb1RxVGdxbFc1c0YvbGU5K0Vhemx4K0JpZldOY0dhSXljNzIyV3BtZEJkcTdTaXo4R3lHNkpscG45aStWYXpWMFJia3ZPUnRsU3AzQ2VnUmhUaVlLQU1nR3NZRlpoV1lKdHFBZWQvaytXL3hGbDVIZmRWRHlxYTMzaG41MGREQm5PclYzZitJQmIxdC84STUvdC9uUUg2NlVJV01LYkVEdnN5dXpJSVROZ0E1NXp6NkNRSEh2NXFuOHlJMUVFUU9BV1FCOXI3bEJFRGlrQjJiNDBkQ3AxV0paczEvaWZHbU5LK0pmb1V3WVdpVFBLVG84bEo2V2I2aDFkZUw2RENaWHA1MURuY0ZIN0NzMVhRaWp4V1lvcW5tL0E0TlQyY0dianM3ejBPMUpWYUFJeTVPcFo4T3dNcEM4cVFsRS9xVDRhcmVQWkhMbUw3S0tHUmNQWFRIRHNZa0NJcDJuZzBQSFJIeGZhdy9mODVDeGFoOW9sWmx0dHNlSXhPSGlFYUFBNU81U1ppQUs2QXExK2lKS2R2V2duVXlwQ0NKVzU3OGlDYWp1aHhpbVljT3gveEdOQnRNOWgxUjVzVFFEWnhlMU5qSmpkaHh0OVhIM01xZzFVWXQ0WDg9
```
Response:
```
{"error":0,"data":{"type":"ad-info","inzerazId":1,"id":43,"url":"http:\/\/napredaj.local\/ad\/43","status":"live","expire":1409735027,"featured":false,"viewsCount":null},"token":"cTlDRW5tTm9iU091d1lES3FwM0pCNzJrdDViOEtGNzZoRFJD"}
```

### Zmazanie inzeratu
Request:
```
http://napredaj.local/api/save-ad?action=deactivate-ad&token=c2VadGNwVHFqQTdGRHVTMlVZMmdDQXdtZUFaMHNtWlNFeVli&data=RDQ5U3d3TWk3a1UwZGsvUFVNNHl2dz09
```
Response:
```
{"error":0,"data":[{"type":"ad-info","inzerazId":4,"status":"not-exists"},{"type":"ad-info","inzerazId":2,"status":"not-exists"}],"token":"UXZyWG1JRVVsTFA1cjBab3VOc2lKcFRJaFlIdS9sNUlENFZT"}
```

### Ziskanie info o inzerate
Request:
```
http://napredaj.local/api/save-ad?action=ad-info&token=ZTNRVUhyQzlqcUhMbXNWMEtoQS93UDNQVGpkNkVyMlA2ZnpS&data=ZWFmV1RCcUhybDhlS1g3cGFmV2FXQT09
```
Response:
```
{"error":0,"data":[{"type":"ad-info","inzerazId":1,"id":42,"url":"http:\/\/napredaj.local\/ad\/42","status":"draft","expire":1409725198,"featured":false,"viewsCount":13},{"type":"ad-info","inzerazId":5,"status":"not-exists"}],"token":"UXZyWG1JRVVsTFA1cjBab3VOc2lKcFRJaFlIdS9sNUlENFZT"}
```
