<?php

namespace InzerazTests;


use Tester\Assert;

require __DIR__ . '/bootstrap.php';



test(function() {

	$parameters = array(
		'token' => '',
		'data' => '',
		'action' => ''
	);

	Assert::equal(false, \Inzeraz::verifyRequest($parameters));


	$parameters = array(
		'token' => '506df11917b4b68cdebbb8640cd5968cedd43696',
		'data' => json_encode(array('foo', 'bar')),
		'action' => \Inzeraz::ACTION_SAVE_AD
	);

	Assert::equal(array('foo', 'bar'), \Inzeraz::verifyRequest($parameters));


});
