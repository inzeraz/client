<?php

namespace InzerazTests;


use Tester\Assert;

require __DIR__ . '/bootstrap.php';



test(function() {

	Assert::equal('347e87460c4a2b80fd3fb5b1348610e4fc79e50e', \Inzeraz::getToken('foo bar'));
	Assert::equal('506df11917b4b68cdebbb8640cd5968cedd43696', \Inzeraz::getToken(json_encode(array('foo', 'bar'))));
	Assert::equal('5cc6c9db437d2438884ced5f967ef3d1ae4c55ab', \Inzeraz::getToken('N3FzWUR3R1c3eCttaVR5aWl3WVljZz09'));

});


test(function() {

	Assert::equal(true, \Inzeraz::isTokenValid('347e87460c4a2b80fd3fb5b1348610e4fc79e50e', 'foo bar'));

});
