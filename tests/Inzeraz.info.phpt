<?php

namespace InzerazTests;


use Tester\Assert;

require __DIR__ . '/bootstrap.php';



test(function() {

	$info = \Inzeraz::adInfo(1, 1, 'http://host.com/1', \Inzeraz::AD_STATUS_DRAFT);

	$expected = array(
		'type' => 'ad-info',
		'inzerazId' => 1,
		'id' => 1,
		'url' => 'http://host.com/1',
		'status' => 'draft',
		'expire' => NULL,
		'featured' => FALSE,
		'viewsCount' => NULL,
	);

	Assert::equal($expected, $info);

});


test(function() {

	$info = \Inzeraz::adNotExistsInfo(1);

	$expected = array(
		'type' => 'ad-info',
		'inzerazId' => 1,
		'status' => 'not-exists',
	);

	Assert::equal($expected, $info);

});
